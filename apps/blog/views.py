from django.shortcuts import render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from .models import Post

def tagpage(request, tag):
	posts = Post.objects.filter(tags__name=tag)
	return render_to_response("tagpage.html", {"posts":posts, "tag":tag})
	
def about(request):
	return render(request, 'acerca.html')
	
def contact(request):
	return render(request, 'contacto.html')