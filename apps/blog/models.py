from django.db import models
from taggit.managers import TaggableManager
from markdown import markdown

class Post(models.Model):
	title = models.CharField(max_length=200)
	created = models.DateTimeField(db_index=True)
	slug = models.SlugField(max_length=40, unique=True)
	body = models.TextField()
#	body_html = models.TextField(editable=False, blank=True, null=True)

	tags = TaggableManager()	

#	def save(self):
#            self.body_html = markdown(self.body, ['codehilite'])
#            super(Post, self).save()

	def __unicode__(self):
		return self.title