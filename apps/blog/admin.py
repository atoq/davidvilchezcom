from django.contrib import admin
from .models import Post

class AdminEntries(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['title'] }


admin.site.register(Post)
#admin.site.register(models.Post, PostAdmin)