from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.http import HttpResponsePermanentRedirect
from django.conf.urls.defaults import patterns, include, url
from django.views.generic import ListView, DetailView
from .models import Post
import views

urlpatterns = patterns('apps.blog.views',
 	url(r'^$', ListView.as_view(
				queryset=Post.objects.all().order_by("-created")[:4],
				template_name="blog.html")),
	url(r'^(?P<pk>\d+)$', DetailView.as_view(
                                model=Post,
                                template_name="post.html")), 

                                
	url(r'^archivos/$', ListView.as_view(
				queryset=Post.objects.all().order_by("-created"),
                                template_name="archives.html")),
	url(r'^tag/(?P<tag>\w+)/$', 'tagpage'),
#	url(r'^acerca/$', lambda x: HttpResponsePermanentRedirect('/acerca/')),
	url('^acerca/$', 'about'),
	url('^contacto/$', 'contact'),
#	url(r'^acerca/$', ListView.as_view(
#				template_name="acerca.html")),
)
